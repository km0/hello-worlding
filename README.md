# Hello Worlding - Pathways through code documentation

![People in a dangerous situation because they didn't read the readme](cover.jpg)

_Hello Worlding_ is a network of pathways to walk through different ways to read, write and think with code documentation.

Each pathway is a reader made of READMEs: a collection of text files that usually accompany software to provide insights and explanations about what's going on in the code. Each pathway focuses on a particular approach or method to highlight the potential of code documentation to create worlds around the making of software.

## Here some context

The nature of code documentation is to create entry points for people to participate in programming practices. To encode and filter knowledge, and ultimately to share it with others. This "nature", however, does not come without issues. It makes a lot of assumptions about who's reading, expecting experts, or engineers, or dudes. Its language is unwelcoming: too dense, too technical, very gendered and unable to address anyone but the neurotypical-white-cis-male programmer. Documentation requires an enormous amount of care, energy and time to be maintained, and it's done always out of budget, always as a side project, always at the end, and only if there's time left.

Even by doing a questionable job at creating entry points, code documentation still has a lot of potential as a backdoor. It's a publishing surface whose reach extends through time and space. Time because it meets programmers at different moments in their lives: from the _hello world_ till the _how to uninstall_, and it influences thinking about software continuously. Space because it comes in many different possible formats, and can shapeshift to serve different occasions: from simple .txt files to entire websites, from coding workshops to comments in the source code to series of video tutorial.

The question then becomes: can we make use of these backdoors to infiltrate programming practices and open more entry points from within?

A README is an interface between the code, the user, the developer, and the world. Living close to the source code, but at the same time being less rigid and more expressive, it seems to be an ideal surface to influence software development practices. A space with potential to renegotiate and reclaim given margins and entry points. A chance to overwrite what is normalized, and let more voices participate in the discourse that is software. A way to produce narrations around software. To create a world for the code to inhabit, to stretch what is possible to do or to think with it. Documentation is a space for the political in the software. A surface that could host ideas in close contact with code, letting them entangle and shape each other.

## Here some background

This project grows out of the contradiction of being frustrated in having to deal with undocumented pieces of software, and at the same time never documenting anything. While for sure there is some thrills in understanding how to stitch codes together, the lack of documentation poses a great hindrance for the participation of diverse knowledges in the making of software. At the same time, this very lack of documentation could be used as a starting point.

## Here some technical details

Hei, I'm not really sure about this setup, but think it's worth trying. The plan is to stay simple but expressive, and have fun during development.

### Structure of the frontend:

-   Homepage
    -   intro text
    -   list of pathways
    -   list of all readmes
        -   readme is a link + descriptions of why is special
-   Pathway page
    -   intro
    -   list of pathway's readmes
        -   again readme is a title, link and description of why is special
-   About page
    -   Longer description, the one used in the readme here could be a good starting point. Need to stress a bit more the idea of pathway.
    -   Info on how to collaborate, link to repo, license, clerical stuff
-   Activation page ????????????
    -   text on this idea of snapshot in relation to pathways
    -   documentation of snapshot
    -   can host documentation and info about workshops etc.

Think about it as a writing machine! To say: ok now i'm working on this pathway that is focused on the usage of a certain word, It can be a process to annotate how this usage change over time for example.

Note that: a readme can appear in different pathways for different reasons!
And that's why we are talking about network of pathways.

## Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
