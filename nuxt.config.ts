import svgLoader from "vite-svg-loader";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	devtools: { enabled: false },
	css: ["~/assets/css/fonts.css", "~/assets/css/variables.css", "~/assets/css/global.css"],
	modules: ["@vueuse/nuxt", "@nuxt/content"],
	postcss: {
		plugins: {
			"postcss-nested": {},
			"postcss-import": {},
		},
	},
	vite: {
		plugins: [svgLoader({})],
	},
});
