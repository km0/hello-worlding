---
title: Openings
description: Reading just the very first sentence
tags:
    - Reading
    - Writing
date: 22/05/2023
links:
    - title: Opening - Tag
      url: https://hub.xpub.nl/soupboat/tag/02/
---

![The opening of Flask documentation, with some words erased](/img/openings.png)

Like in the game of chess, an opening in code documentation is a statement.

An opening is a declaration of intents, it manifests intentions. These intentions are glued together in the smallest context possible of a sentence. They are like a trailer for a movie, or the thumbnail of a youtube video. Often exagerate, click-bait, ideological. How and why these different facets come together? They are a visibile traces of choices made during development, choices that sometimes are made evident within the documentation, and sometimes remain unanswered.

From the perspective of someone writing code and therefore code documentation, I'm interested in exploring these openings both from a stylistic and technical point of view. Leveraging on form, they are a perfect device to set the stage for code: a way to illuminate code from a particular angle, revealing some aspects and conceiling other ones. From a technical perspective they attach onto already existent systems of meaning, such as particular technologies, a programming languages, frameworks, or coding paradigms. They trigger mutual influences between code and context, like a telescope that can be used from both sides: from the code to the bigger context, and from the bigger context to the code. A way to encode context in the very first lines of documentation.

As someone that reads code documentation, I'm interested in making these openings more eloquent. Or even just having some tool or practice to decode them. Inflating the opening, even in directions that differ from the rest of the documentation. To see connections and genealogies, relations and contradictions in broader ecosystems.

Two keywords: encode and decode [context].
These could also be a set of different practices. With different tempo.

Opening as maneuverable object.

Something maneuverable for who writes.  
Something maneuverable for who reads.  
Maneuverable meaning?  
Meaning small changes with visible effects.
Meaning interactive?  
Moire pattern  
Shift in perspective
