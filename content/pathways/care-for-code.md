---
title: Care for code
description: Prompts to start writing code documentation.
date: 20/03/2023
tags:
    - Pubblication
    - Workshop
links:
    - title: Care for Code
      url: https://hub.xpub.nl/soupboat/~kamo/care_for_code/
    - title: Documentation session
      url: https://hub.xpub.nl/soupboat/docs/
    - title: Varia
      url: https://vvvvvvaria.org/code-documentation.html
---

A small zine prepared in occasion of the second [Documentation session](https://hub.xpub.nl/soupboat/docs/), hosted at [Varia](https://vvvvvvaria.org/code-documentation.html), Rotterdam on March 23rd.

The fear of the blank readme file is a great challenge to overcome when writing docs. The pubblication takes into account several departing points from where to start documenting code.

Find the zine online on the Soupboat, a self-hosted server mantained within the context of the Experimental Pubblishing master of the Piet Zwart Institute, Rotterdam.

Download it here, in a print-friendly web-to-print booklet:  
[Care for Code](https://hub.xpub.nl/soupboat/~kamo/care_for_code/)

![Scan of the first page of the booklet](/img/cfc1.jpg)

![Scan of the booklet, with two different drawing approaches to code documentation](/img/cfc2.jpg)
