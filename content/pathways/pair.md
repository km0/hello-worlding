---
title: Pair documenting
description: Write and read together with someone else
tags:
    - Activity
date: 24/04/2023
links:
    - title: Pair - Tag
      url: https://hub.xpub.nl/soupboat/tag/03/
---

The practice of pair programming involves two developers coding together on the same machine, with distinct roles. The driver writes the actual code, while the navigator reviews each entry along the way. Roles are often switched. Developers with similar or different experience can collaborate in pair programming, with benefits for both parts.

Navigating through documentation can be difficult. Could the concept of pair programming be applied to documentation?

**Pair documentation writing:** one writes and the other provides perspective, such as reviewing if there are parts that are not clear

**Pair documentation reading:** one read and the other ???

What could it mean to read documentation in pair?

> Documentation is not just for beginners, is a code companion. One never stops reading. Both newcommers and experienced developers alike read documentation, that meets them in different moment of their programmer lives, from the hello world to the how to uninstall

When pairing reading make sense? Which kind of situations?

One read, the other document the reading process: keeping track of the screen walk, annotating resources, drating a map of where knowledge has been found.

I'm struggling because all of these things are activities and not objects and this cause me disconfort.

## Start with someone else!

Teaming up with others comes with several benefits!

Teaming up reduces the workload on the individual: writing docs is demanding and delicate. It requires focus and care and energies: it's like gardening, or orthopedics, or cooking, or policymaking.

Teaming up offers multiple pairs of eyes to keep track of code. Here, even before releasing the docs, it's necessary to find a middle ground between different perspectives. Beginner and experienced programmers can write docs together! The process is beneficial for both, and the resulting docs comes with more facets.

Teaming up comes handy to undo assumptions and to open more entry points. A shared effort is already a moment of publishing. Making meaning together. A way to join different knowledges and skills, to broaden participation in the making of software.

An excerpt from [Care for code](/pathways/care-for-code)
