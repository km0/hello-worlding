---
title: Documentation Sessions
description: 2 hours of headspace to write some nice code documentation
date: 15/03/2023
tags:
    - Workshop
    - Events
links:
    - title: Documentation Sessions
      url: https://hub.xpub.nl/soupboat/docs/
    - title: Varia
      url: https://vvvvvvaria.org/code-documentation.html
---

Sometimes the only thing we need it's some dedicated time. It's always easy to postpone something perceived as marginal as writing some docs: there are always more important, urgent, compelling things to do first.

The format of the documentation session addresses this difficulty in making time: it sustains commitment by creating a common bubble of time, a shared space, a sociality around the making of software.

The format is loose enough to let different kinds of participation: someone could come with a specific project in mind, someone else with a more explorative approach. Someone could write together with others, or just work at the same table on different pieces.

The amount of facilitation depends on the context where the session is hosted. People more familiar with code docs wont probably need much more prompts, while others more green and at the beginnings could enjoy some advices.

For example: a session could be dedicated in writing a welcoming README, focusing on the phrasing and undoing general assumptions technical documentation often manifests.

Here you can find the website: [Documentation Sessions](https://hub.xpub.nl/soupboat/docs/).

![Screenshot of the website for the Documentation Sessions](/img/docs.webp)
Screenshot of the Documentation Sessions website.

## An invitation

Here is an invitation for the second session, hosted in Varia on March 23rd.

> Let's write some code documentation! For your new coding project, for the cryptic library you downloaded recently, for a script that you want to share with others! <br><br>
> Too many pieces of code are left alone out there: without an entry point, forgotten, while outside is raining. Wouldn't it be nice to take care of them? <br><br>
> Enter the documentation sessions. Two hours where to sit with source code and write something about it. From simple instructions to in-depth explanations, or maybe some drawings to illustrate the overall process. You name it. <br><br>
> Writing documentation is difficult! But let's face this together: prompts and suggestions will be offered for inspiration, and coffee and snacks for restoration. If you want there will be space to share your work and exchange feedback, if not no prob: just enjoy the cozy music and write some docs.<br>  
> **Practical info:** Two hours on \_\_, **starting at** \_\_! For people that thinkers with code, no matter the experience. Processing, P5.js, TouchDesigner, Javascript, Python, doesn't really matter! All code looks better when is documented! Come alone or with friends, bring your laptop and something to document.
