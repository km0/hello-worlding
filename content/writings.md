---
title: Hello Worlding
description: Code documentation as entry point / backdoor to programming practice
---

You cannot think about code without thinking also about code documentation.

Programming is like walking in a room without turning the lights on. It can be a place you know by heart, but you still prefer to rely on some guidance, using your hands to sense the walls, and move through the furniture without hitting your pinkie toe. For code is the same: you usually appreciate some help.

Documentation can be as simple as a plain text file placed near the code. A README.txt that invites developers to take a look before diving into the program. Printed technical manuals have today transformed and spread into many different shapes: wikis and platforms and websites generated with various tools, each with particular focus and features.

This research explores two currents around documentation practices, with the thesis that code documentation is an ideal publishing surface to create worlds around software, and to orientate software in the world.

**Current #1** Documentation broadens participation in the making of software: lowering barriers and offering entry points for people to understand and attune with code.

**Current #2** Documentation as a backdoor where to inject context into software: to host principles in close contact with algorithms, letting them entangle and shape each other.

The nature of code documentation is to create entry points for people to participate in programming practices. To encode and filter knowledge, and ultimately to share it with others. This "nature", however, does not come without issues. It makes a lot of assumptions about who's reading, expecting experts, or engineers, or dudes. Its language is unwelcoming: too dense, too technical, very gendered and unable to address anyone but the neurotypical-white-cis-male programmer. Documentation requires an enormous amount of care, energy and time to be maintained, and it's done always out of budget, always as a side project, always at the end, and only if there's time left. The first chapter raises these points to note how often code documentation acts as a barrier, gatekeeping access to the making of software.

Even if it does a questionable job at creating entry points, code documentation still has a lot of potential as a backdoor. It's a publishing surface whose reach extends through time and space. Time because it meets programmers at different moments in their lives: from the hello world till the how to uninstall, and it influences thinking about software continuously, and from different perspectives. Space because it comes in many different possible formats, and can shapeshift to serve different occasions: from simple .txt files to entire websites, from coding workshops to comments in the source code to series of video tutorial.

The question then becomes: can we make use of these backdoors to infiltrate programming practices and open more entry points from within?
Code documentation is an interface between the code, the user, the developer, and the world. Living close to the source code, but at the same time being less rigid and more expressive, it seems to be an ideal surface to influence software development practices. The second chapter presents some examples of how documentation can be used to orientate code in the world, addressing politics of participation, representation, and authorship in programming. The case studies come from diverse realities, and from different scales: large collaborative projects as well as small, personal gestures. In their multiplicity, they show how blurred the boundaries of code documentation are. A lack of fixedness which in turn can be used to mold our wishes and values into it.
A term to contextualize (and dismantle?) in these writings is developer. Stripping away any trace of professionalism and formal education, let's agree that a developer is someone who tinkers with code. No matter what level, nor distance, nor experience. No matter if for work, for fun, for study. No matter if one is actively involved in the development of a tool, or comes from the user perspective. Ellen Ullman writes that programming is a disease, but the situation is even worse: code is contagious material, touch it once, and you are a developer.

Continue reading on [Soupboat](https://hub.xpub.nl/soupboat/~kamo/thesis/)
